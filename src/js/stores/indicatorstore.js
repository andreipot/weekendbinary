'use strict';

import Actions from '../actions/actions';
import isEqual from '../utils/isequal';

class IndicatorStore {
	constructor() {
		this.channels = {};
		this.bindAction(Actions.initApp, IndicatorStore.addChannels);
		this.bindAction(Actions.setIndicator, this.setIndicator);
	}

	static addChannels(mainsections) {
		mainsections.map(m => {
			return (this.channels[m.symbol] = {
				//indicatorSettings: {},
				chartIndicators: []
			});
		});
		return false;
	}

	setIndicator(paramsObj) {
		let symbol = paramsObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		let indicator = paramsObj.indicator,
			base = paramsObj.base,
			period = paramsObj.period,
			type = paramsObj.type,
			separate = (['RSI', 'ATR', 'CCI', 'ADX', 'MACD'].indexOf(indicator) > -1),
			newIndicator = {indicator, base, period, type, separate},
			separatePaneBusy = null;
		if (indicator === undefined) {
			// clear all indicators
			chan.chartIndicators = [];
			return;
		}
		if (!chan.chartIndicators.length) {
			// no indicators yet
			chan.chartIndicators = [newIndicator];
			return;
		}
		for (let i = 0, z = chan.chartIndicators.length; i < z; i++) {
			// ignore if submitted indicator exists already
			if (isEqual(newIndicator, chan.chartIndicators[i])) return false;
			if (chan.chartIndicators[i].separate && separate) {
				separatePaneBusy = i;
			}
		}

		if (indicator === 'BB') {
			// clear main pane
			chan.chartIndicators = (chan.chartIndicators.filter(function (v) {
				return v.indicator !== 'MA' && v.indicator !== 'BB';
			}));
		}

		if (indicator === 'MA') {
			// clear main pane from BB
			chan.chartIndicators = (chan.chartIndicators.filter(function (v) {
				return v.indicator !== 'BB';
			}));
		}

		if (separatePaneBusy !== null) {
			// replace existing indicator in the separate pane
			chan.chartIndicators[separatePaneBusy] = newIndicator;
		} else {
			chan.chartIndicators.push(newIndicator);
		}

		/*let currentSeparateIndicator = (chan.chartIndicators.filter(function (v) {
		 return v.separate === true;
		 }))[0];
		 console.log(currentSeparateIndicator, separate);*/
		/*if (indicator === 'MA') {
		 chan.chartIndicators = (chan.chartIndicators.filter(function (v) {
		 return v.indicator === 'MA';
		 }));
		 if (chan.chartIndicators.length >= 2) chan.chartIndicators.pop();
		 chan.chartIndicators.unshift(newIndicator);
		 } else {
		 chan.chartIndicators = [newIndicator];
		 }*/
	}
}

export default alt.createStore(IndicatorStore, 'IndicatorStore');
