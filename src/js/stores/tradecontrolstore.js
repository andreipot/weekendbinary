'use strict';
// todo - try immutable state here

import Actions from '../actions/actions';
import InitConfig from '../utils/initconfig';


class TradeControlStore {
	constructor() {
		this.channels = {};
		this.bindAction(Actions.initApp, TradeControlStore.addChannels);
		this.bindAction(Actions.setExpiryValue, this.setExpiryValue);
		this.bindAction(Actions.setAmountValue, this.setAmountValue);
	}

	static addChannels(appSymbols) {
		if (!appSymbols.length) return false;
		appSymbols.map(m => {
			Actions.subscribeToQuoteFeed(m.symbol);
			return (this.channels[m.symbol] = {
				symbol: m.symbol,
				alias: m.alias,
				expiry: InitConfig.INIT_EXPIRY,
				amount: InitConfig.INIT_AMOUNT
			});
		});
	}

	setExpiryValue(valSymObj) {
		let symbol = valSymObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		chan.expiry = Number(valSymObj.value);
	}

	setAmountValue(valSymObj) {
		let symbol = valSymObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		chan.amount = Number(valSymObj.value);
	}
}

export default alt.createStore(TradeControlStore, 'TradeControlStore');
