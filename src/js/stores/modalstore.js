'use strict';

import Actions from '../actions/actions';

class ModalStore {
	constructor() {
		this.state = {
			isOpen: false
		};
		this.bindAction(Actions.openModal, this.openModal);
		this.bindAction(Actions.closeModal, this.closeModal);
	}

	openModal() {
		this.setState({isOpen: true});
	}

	closeModal() {
		this.setState({isOpen: false});
	}
}

export default alt.createStore(ModalStore, 'ModalStore');
