'use strict';

import Actions from '../actions/actions';
import InitConfig from '../utils/initconfig';
import fmtMoney from '../utils/fmtmoney';
import earlyExitCalc from '../utils/earlyexitcalc';

class TradeTableStore {
	constructor() {
		this.activeView = 'opentrades';
		this.openTrades = [];
		this.closeTrades = [];
		this.selectedTradeIndex = null;
		this.tradeResult = null;
		this.userIn = false;
		this.bindAction(Actions.setTradeTableView, this.setTradeTableView);
		this.bindAction(Actions.openTrade, this.openTrade);
		this.bindAction(Actions.closeTrade, this.closeTrade);
		this.bindAction(Actions.updateTicker, this.updateOpenTrades);
		this.bindAction(Actions.setSelectedTrade, this.setSelectedTrade);
		this.bindAction(Actions.userLoggedIn, this.userLoggedIn);
		this.bindAction(Actions.getUserOpenTrades, this.getUserTrades);
		this.bindAction(Actions.getUserClosedTrades, this.getUserTrades);
		this.exportPublicMethods({
			getActiveTrade: this.getActiveTrade
		});
	}

	setTradeTableView(valSymObj) {
		this.activeView = valSymObj.value;
	}

	openTrade(tradeData) {
		if (!this.userIn) return false;
		let shift = 3;
		this.selectedTradeIndex = (this.openTrades.push({
				'ticket': tradeData.order,
				'symbol': tradeData.symbol,
				'bet amount': fmtMoney(tradeData.amount),
				'payout': fmtMoney(tradeData.amount * tradeData.payout),
				'roi': InitConfig.ROI + '%',
				'type': (tradeData.flags === InitConfig.CALL_FLAG) ? 'CALL' : 'PUT',
				'open': this.roundToShift(tradeData.price_open, shift),
				'expiry': tradeData.expiration_in,
				'profit/loss': '',
				'time': tradeData.expiration_in,
				//'current': 0,
				'expiration': tradeData.expiration,
				'flags': tradeData.flags,
				'open time': tradeData.time_open
			})) - 1;
		this.tradeResult = null;
		this.activeView = 'opentrades';
		console.log('%c trade ' + tradeData.order + ' opened ' + new Date(tradeData.time_open).toLocaleString(), 'background: green; color: white');
	}

	closeTrade(tradeData) {
		if (!this.userIn) return false;
		let tradeResult = tradeData.profit,
			openTrades = this.openTrades,
			closeTrades = this.closeTrades,
			currTrade,
			currTradeResult,
			eeTime;
		for (let i = 0, z = openTrades.length; i < z; i++) {
			currTrade = openTrades[i];
			if (currTrade.ticket === tradeData.order) {
				currTradeResult = tradeResult - tradeData.amount;
				eeTime = this.getEETime(tradeData.expiration_in - ((tradeData.time_close - tradeData.time_open) / 1000));
				closeTrades.push({
					'ticket': tradeData.order,
					'symbol': tradeData.symbol,
					'bet amount': currTrade['bet amount'],
					'payout': currTrade.payout,
					'roi': currTrade.roi,
					'type': currTrade.type,
					'open': tradeData.price_open,
					'close': tradeData.price_close,
					'expiry': currTrade.expiry,
					'open time': tradeData.time_open,
					'close time': tradeData.time_close,
					'EE': eeTime,
					'profit/loss': fmtMoney(currTradeResult)
				});
				openTrades.splice(i, 1);
				window.setTimeout(() => {
					Actions.updateBalance(currTradeResult + Number(currTrade['bet amount'].replace(/[^0-9\.-]+/g, '')));
				});
				if (!openTrades.length) {
					this.selectedTradeIndex = null;
				} else {
					if (this.selectedTradeIndex > (openTrades.length - 1)) this.selectedTradeIndex = openTrades.length - 1;
					if (openTrades[this.selectedTradeIndex].symbol !== tradeData.symbol) {
						// switch app activeSymbol
						window.setTimeout(() => {
							Actions.setActiveSymbol(openTrades[this.selectedTradeIndex].symbol);
						});
					}
				}
				if (this.tradeResult === null) {
					this.tradeResult = currTradeResult;
					window.setTimeout(() => {
						this.tradeResult = null;
						this.emitChange();
					}, 3000);
				}

				console.log('%c trade ' + tradeData.order + ' closed ' + new Date(tradeData.time_close).toLocaleString() + ', ' + fmtMoney(currTradeResult), 'background: blue; color: white');
				break;
			}
		}
	}

	updateOpenTrades(quoteData) {
		if (!this.userIn) return false;
		if (!this.openTrades.length) return false;
		let openTrades = this.openTrades,
			currTrade;
		for (let i = 0, z = openTrades.length; i < z; i++) {
			currTrade = openTrades[i];
			if (currTrade.symbol === quoteData.symbol) {
				let shift = 3,
					currRound,
					tradeRounds,
					bid = this.roundToShift(quoteData.bid, shift),
					timestamp = quoteData.time,
					amount,
					currentCandleValue,
					earlyExitPayout;
				currTrade.tradeSelected = (i === this.selectedTradeIndex);
				currRound = Math.floor((timestamp - currTrade['open time']) / 1000) + 1;
				if (currRound <= 0 || currRound >= currTrade.expiry) continue;
				tradeRounds = currTrade.expiry;
				currTrade.time = tradeRounds - currRound;
				amount = Number(currTrade['bet amount'].replace(/[^0-9\.-]+/g, ''));
				//currTrade.current = bid;
				currentCandleValue = Math.round((bid - Number(currTrade.open)) / Math.pow(0.1, shift));
				earlyExitPayout = ((earlyExitCalc(currentCandleValue, currTrade.flags === InitConfig.CALL_FLAG, tradeRounds, currRound) * amount) - amount);
				if (Math.abs(earlyExitPayout) > amount) earlyExitPayout = amount * (earlyExitPayout < 0 ? -1 : 1);
				currTrade['profit/loss'] = fmtMoney(earlyExitPayout);
				//console.log('EE:', earlyExitPayout, 'val:', currentCandleValue, 'betUP:', currTrade.flags === InitConfig.CALL_FLAG, 'round:', currRound + '/' + currTrade.expiry);
			}
		}
	}

	setSelectedTrade(tradeData) {
		if (!this.userIn) return false;
		let openTrades = this.openTrades,
			currTrade;
		//if (openTrades.length < 2) return false;
		for (let i = 0, z = openTrades.length; i < z; i++) {
			currTrade = openTrades[i];
			if (tradeData.tradeId === currTrade.ticket) {
				this.selectedTradeIndex = i;
				break;
			}
		}
	}

	userLoggedIn(userData) {
		this.userIn = userData ? true : false;
		this.activeView = 'opentrades';
		this.openTrades = [];
		this.closeTrades = [];
		this.selectedTradeIndex = null;
	}

	getUserTrades(tradesData) {
		if (!this.userIn) return false;
		let trades = tradesData,
			symbol,
			currTrade,
			ticket,
			betAmount,
			payout,
			roi = InitConfig.ROI + '%',
			type,
			open,
			close,
			expiry,
			timeOpen,
			timeClose,
			eeTime;
		if (!trades.length) {
			Actions.setLoading(false);
			return false;
		}
		for (let i = 0, z = trades.length; i < z; i++) {
			currTrade = trades[i];
			symbol = currTrade.symbol;
			ticket = currTrade.order;
			betAmount = fmtMoney(currTrade.amount);
			payout = fmtMoney(currTrade.amount * currTrade.payout);
			type = (currTrade.flags === InitConfig.CALL_FLAG) ? 'CALL' : 'PUT';
			open = currTrade.price_open;
			close = currTrade.price_close;
			expiry = currTrade.expiration_in;
			timeOpen = currTrade.time_open;
			timeClose = currTrade.time_close;
			if (timeClose) {
				// closed trade
				eeTime = this.getEETime(expiry - ((timeClose - timeOpen) / 1000));
				this.closeTrades.push({
					ticket,
					symbol,
					'bet amount': betAmount,
					payout,
					roi,
					type,
					open,
					close,
					expiry,
					'open time': timeOpen,
					'close time': timeClose,
					'EE': eeTime,
					'profit/loss': fmtMoney(currTrade.profit || currTrade.amount * -1)
				});
			} else {
				this.selectedTradeIndex = (this.openTrades.push({
						ticket,
						symbol,
						'bet amount': betAmount,
						payout,
						roi,
						type,
						open,
						expiry,
						'profit/loss': '',
						'time': expiry,
						'expiration': currTrade.expiration,
						'flags': currTrade.flags,
						'open time': timeOpen
					})) - 1;
			}
		}
		Actions.setLoading(false);
	}

	roundToShift(val, shift) {
		if (!isNaN(val)) {
			let mult = Math.pow(10, shift);
			return (Math.round(val * mult) / mult);
		} else {
			return val;
		}
	}

	getEETime(tradeTime) {
		switch (true) {
			case tradeTime < 0:
				return '';
			case tradeTime < 1:
				return 1;
			default:
				return Math.floor(tradeTime);
		}
	}

	getActiveTrade = () => {
		let selectedTrade = (this.selectedTradeIndex !== null) ? this.openTrades[this.selectedTradeIndex] : {},
			tradeResult = this.tradeResult;
		return {selectedTrade, tradeResult};
	}
}

export default alt.createStore(TradeTableStore, 'TradeTableStore');
