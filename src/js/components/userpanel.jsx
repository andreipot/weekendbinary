'use strict';

import Actions from '../actions/actions';
import fmtMoney from '../utils/fmtmoney';

export default class UserPanel extends React.Component {
	static displayName = 'UserPanel';

	static propTypes = {
		user: React.PropTypes.object.isRequired
	};

	static defaultProps = {
		user: {}
	};

	constructor(props) {
		super(props);
	}

	render() {
		let content = this.props.user.username ?
			<dt>
				<div className="yell">{this.props.user.username}</div>
				<div>Balance:&nbsp;
				<span className={this.props.user.userbalance > 0 ? 'up' : 'down'}>{fmtMoney(this.props.user.userbalance)}</span>
					</div>
				<button className="btn redhover" onClick={()=>{Actions.userLogout();}}>Logout</button>
			</dt>
			:
			<form onSubmit={this.userLogin}>
				<input type="text" name="username" placeholder="Login" maxLength="30"/>
				<input type="password" name="password" placeholder="Password" maxLength="30"/>
				<button className="btn" type="submit">Login</button>
			</form>;
		return (
			<div className="userPanel">
				<div className="inner">
				{content}
				</div>
			</div>
		);
	}

	userLogin = (e) => {
		e.preventDefault();
		let form = e.target,
			auth = form.username.value,
			password = form.password.value;
		if (auth && password) Actions.userLogin({auth, password});
	};
}
