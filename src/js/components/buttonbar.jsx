'use strict';

import classNames from '../utils/classnames';

export default class ButtonBar extends React.Component {
	static displayName = 'ButtonBar';

	static propTypes = {
		activeSymbol: React.PropTypes.string,
		btnAction: React.PropTypes.func,
		buttons: React.PropTypes.array.isRequired,
		activeBtn: React.PropTypes.string,
		name: React.PropTypes.string.isRequired
	};

	constructor(props) {
		super(props);
	}

	shouldComponentUpdate(nextProps) {
		return (nextProps.activeBtn !== this.props.activeBtn);
	}

	handleClick = (e) => {
		e.preventDefault();
		let btn = e.currentTarget;
		if (btn.classList.contains('active')) return;
		if (this.props.btnAction) {
			this.props.btnAction({
				symbol: this.props.activeSymbol || null,
				name: this.props.name,
				value: btn.getAttribute('data-value')
			});
		}
	};

	render() {
		let barContent = this.props.buttons.map((m, i)=> {
			let btnClass = classNames(
				'btn',
				{active: (this.props.activeBtn && m.value === this.props.activeBtn)},
				(m.icon) ? 'icon ' + m.icon : ''
			);
			return (
				<div className={btnClass} key={m.value + i} data-value={m.value} onClick={this.handleClick} title={m.title}>{m.label || ''}</div>
			);
		});

		return (
			<nav>
				{barContent}
			</nav>
		);
	}
}
