'use strict';

import Actions from '../actions/actions';
import Store from '../stores/chartcontrolsstore';
import ButtonBar from './buttonbar.jsx';
import Chart from './chart.jsx';
import classNames from '../utils/classnames';

export default class ChartControls extends React.Component {
	static displayName = 'ChartControls';

	static propTypes = {
		activeSymbol: React.PropTypes.string,
		appMode: React.PropTypes.string,
		chartPointerButtons: React.PropTypes.array,
		chartPriceButtons: React.PropTypes.array,
		chartResButtons: React.PropTypes.array,
		chartTypesButtons: React.PropTypes.array
	};

	static defaultProps = {
		chartPriceButtons: [
			{label: 'BID', value: 'Bid', title: 'Select price'},
			{label: 'ASK', value: 'Ask', title: 'Select price'}
		],
		chartResButtons: [
			{label: 'TICK', value: 'Tick', title: '1 second period'},
			{label: 'M1', value: 'M1', title: '1 minute period'},
			{label: 'M5', value: 'M5', title: '5 minutes period'},
			{label: 'M15', value: 'M15', title: '15 minutes period'},
			{label: 'M30', value: 'M30', title: '30 minutes period'},
			{label: 'H1', value: 'H1', title: '1 hour period'},
			{label: 'H4', value: 'H4', title: '4 hours period'},
			{label: 'D1', value: 'D1', title: '24 hours period'}
		],
		chartPointerButtons: [
			{icon: 'point', value: 'point', title: 'Point'},
			{icon: 'measure', value: 'measure', title: 'Measure'}
		],
		chartTypesButtons: [
			{icon: 'candles', value: 'candlestick', title: 'Candlestick chart'},
			{icon: 'spline', value: 'areaspline', title: 'Spline chart'}
		]
	};


	constructor(props) {
		super(props);
		this.state = Store.getState();
	}

	componentDidMount() {
		Store.listen(this.onStateChange);
	}

	componentWillUnmount() {
		Store.unlisten(this.onStateChange);
	}

	render() {
		if(this.state.channels[this.props.activeSymbol] === undefined) return null;
		let chan = this.state.channels[this.props.activeSymbol],
			symbolsBarContent = Object.keys(this.state.channels).map((m)=> {
			let chan = this.state.channels[m],
				symbol = chan.symbol,
				key = 'tab' + symbol,
				btnClass = classNames(
					'btn',
					{active: (symbol === this.props.activeSymbol)}
				);
			return (
				<nav key={key}><div className={btnClass} key={key} data-value={symbol} onClick={this.setActiveSymbol}>{chan.alias + ', ' + chan.chartres || ''}</div></nav>
			);
		});

		return (
			<div className="panel chartPanel">
				<div className="btnbar">
					<div className="flexrow">{symbolsBarContent}</div>
					<div className="vsepar"/>
					{(() => {
						if (this.props.appMode === 'fx') return <div><ButtonBar activeSymbol={this.props.activeSymbol} name="chartprice" buttons={this.props.chartPriceButtons} activeBtn={chan.chartprice} btnAction={Actions.setChartControl}/><div className="vsepar"/></div>
							})()}
					<ButtonBar activeSymbol={this.props.activeSymbol} name="chartres" buttons={this.props.chartResButtons} activeBtn={chan.chartres} btnAction={Actions.setChartControl}/>
					<div className="vsepar"/>
					<ButtonBar activeSymbol={this.props.activeSymbol} name="chartpntr" buttons={this.props.chartPointerButtons} activeBtn={chan.chartpntr} btnAction={Actions.setChartControl}/>
					<div className="vsepar"/>
					<nav>
						<div className="btn icon zoomin" data-value="in" title="Zoom in" onClick={this.setChartZoom} />
						<div className="btn icon zoomout" data-value="out" title="Zoom out" onClick={this.setChartZoom} />
					</nav>
					<div className="vsepar"/>
					<nav>
					<div className="btn icon indicators" data-value="indicators" title="Indicators" onClick={this.openModal}/>
						</nav>
					<div className="vsepar"/>
					<ButtonBar activeSymbol={this.props.activeSymbol} name="charttype" buttons={this.props.chartTypesButtons} activeBtn={chan.charttype} btnAction={Actions.setChartControl}/>
				</div>
				<Chart activeSymbol={this.props.activeSymbol} charttype={chan.charttype} chartres={chan.chartres} chartprice={chan.chartprice} chartpntr={chan.chartpntr} zoomlvl={chan.zoomlvl} shift={chan.shift} askdif={chan.askdif}/>
			</div>
		);
	}

	onStateChange = () => {
		this.setState(Store.getState());
	};

	setActiveSymbol = (e) => {
		e.preventDefault();
		let btn = e.currentTarget;
		if (btn.classList.contains('active')) return;
		Actions.setActiveSymbol(btn.getAttribute('data-value'));
	};

	setChartZoom = (e) => {
		e.preventDefault();
		let symbol = this.props.activeSymbol,
			zoom = e.currentTarget.getAttribute('data-value');
		if(symbol && zoom) Actions.setChartZoom({symbol, zoom});
	};

	openModal = () => {
		if (this.state.channels[this.props.activeSymbol].charttype !== 'candlestick') return;
		Actions.openModal();
	};
}
