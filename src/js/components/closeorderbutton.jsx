'use strict';

import Actions from '../actions/actions';

export default class CloseOrderBtn extends React.Component {
	static displayName = 'CloseOrderBtn';

	static propTypes = {
		data: React.PropTypes.string.isRequired,
		rowData: React.PropTypes.object.isRequired
	};

	render() {
		return (
			<div>{this.props.data}
				<button onClick={this.requestEarlyExit}>exit</button>
			</div>
		);
	}

	requestEarlyExit = () => {
		Actions.requestEarlyExit(this.props.rowData.ticket);
	};
}
