'use strict';

import Actions from '../actions/actions';
import Store from '../stores/appstore';
import LinkBar from './linkbar.jsx';
import UserPanel from './userpanel.jsx';
import ChartControls from './chartcontrols.jsx';
import TradeControls from './tradecontrols.jsx';
import Ticker from './ticker.jsx';
import TradeInfoControls from './tradeinfocontrols.jsx';
import TradesTable from './tradestable.jsx';
import ServerTime from './servertimedisplay.jsx';
import ConnectionStatus from './connectionstatus.jsx';
import Modal from '../components/modal.jsx';
import IndicatorsForm from './indicatorsform.jsx';


export default class App extends React.Component {
	static displayName = 'App';

	constructor(props) {
		super(props);
		this.state = Store.getState();
	}

	componentDidMount() {
		Store.listen(this.onStateChange);
		window.addEventListener('online', this.checkIfOnline);
		window.addEventListener('offline', this.checkIfOnline);
	}

	componentWillUnmount() {
		Store.unlisten(this.onStateChange);
		window.removeEventListener('online', this.checkIfOnline);
		window.removeEventListener('offline', this.checkIfOnline);
	}

	render() {
		return (
			<div>
				<section className="topNavBar">
					<div className="logo"></div>
					<div className="title">Hybrid RNG&trade; Trading Platform - 24/7/365</div>
					<UserPanel user={this.state.user}/>
				</section>

				<section className="mainsec flexrow rowParent">
					<div className="flexChild rowParent">
						<div className="flexChild columnParent">
							<div className="flexChild rowParent">
								<div className="panel leftCtrls">
									<TradeControls activeSymbol={this.state.activeSymbol} user={this.state.user} online={this.state.online}/>
								</div>
								<ChartControls activeSymbol={this.state.activeSymbol} appMode={this.state.appMode}/>
							</div>
							<TradesTable />
						</div>
					</div>
					<div className="tickerpanel panel">
						<Ticker activeSymbol={this.state.activeSymbol}/>
						<TradeInfoControls />
					</div>
				</section>
				<section className="bottomNavBar btnbar rowParent flexrow">
					<ServerTime />
					<LinkBar />
					<ConnectionStatus online={this.state.online}/>
				</section>

				<Modal transitionName="modal-anim" width="450px" height="250px">
					<h3>Indicator options</h3>
					<IndicatorsForm activeSymbol={this.state.activeSymbol}/>
				</Modal>
			</div>
		);
	}

	onStateChange = () => {
		this.setState(Store.getState());
	};

	checkIfOnline = () => {
		let symbols = this.state.symbols, i, z;
		if (!window.navigator.onLine) {
			for (i = 0, z = symbols.length; i < z; i++) {
				Actions.subscribeToQuoteFeed(symbols[i].symbol, 'unsubscribe');
			}
			this.setState({online: false});
		} else {
			for (i = 0, z = symbols.length; i < z; i++) {
				Actions.subscribeToQuoteFeed(symbols[i].symbol);
			}
			this.setState({online: true});
		}
	};

}
