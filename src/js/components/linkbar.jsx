'use strict';

export default class LinkBar extends React.Component {
	static displayName = 'LinkBar';

	static propTypes = {
		links: React.PropTypes.array.isRequired
	};

	static defaultProps = {
		links: [
			{title: 'Settings', href: '#'},
			{title: 'Instructions', href: '#'},
			{title: 'My Account', href: '#'},
			{title: 'Support', href: '#'}
		]
	};

	constructor(props) {
		super(props);
	}

	render() {
		var barContent = this.props.links.map((m, i)=> {
			return (
				<a href={m.href} key={'link' + i}>{m.title}</a>
			);
		});

		return (
			<div className="whitetxt">
				{barContent}
			</div>
		);
	}
}
