'use strict';

import App from './app.jsx';
import Store from '../stores/appstore';
import Actions from '../actions/actions';

export default class AppLoader extends React.Component {
	static displayName = 'AppLoader';

	componentDidMount() {
		Store.listen(this.onAppReady);
		Actions.initServer();
	}

	componentWillUnmount() {
		Store.unlisten(this.onAppReady);
	}

	onAppReady = () => {
		React.render(<App />, document.getElementById('app'));
	};

	render() {
		return (
			<div className="loader">
				<div></div>
				<div></div>
				<div></div>
				<div></div>
			</div>
		);
	}
}
