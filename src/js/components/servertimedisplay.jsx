'use strict';

import Store from '../stores/tickerstore';

export default class ServerTimeDisplay extends React.Component {
	static displayName = 'ServerTimeDisplay';

	constructor(props) {
		super(props);
		this.state = {serverTs: Store.getState().serverTs};
	}

	componentDidMount() {
		Store.listen(this.onStateChange);
	}

	componentWillUnmount() {
		Store.unlisten(this.onStateChange);
	}

	shouldComponentUpdate(nextProps, nextState) {
		return (nextState.serverTs !== this.state.serverTs);
	}

	render() {
		return (
			<div>Server Time: {new Date(this.state.serverTs).toLocaleString()}</div>
		);
	}

	onStateChange = () => {
		this.setState({serverTs: Store.getState().serverTs});
	};
}
