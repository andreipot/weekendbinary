'use strict';

export default function fmtMoney() {
    let val = Number(arguments[0]);
    if (isNaN(val)) {
        return '';
    }
    return new Intl.NumberFormat('en-GB', {
            style: 'currency',
            currency: 'GBP',
            minimumFractionDigits: 2
        }).format(Math.round(val + 'e+2') + 'e-2');
}
