/*!
 Copyright (c) 2015 Jed Watson.
 Licensed under the MIT License (MIT), see
 http://jedwatson.github.io/classnames
 */
'use strict';

export default function classNames() {
	var classes = '';

	for (var i = 0; i < arguments.length; i++) {
		var arg = arguments[i];
		if (!arg) continue;

		var argType = typeof arg;

		if (argType === 'string' || argType === 'number' ) {
			classes += ' ' + arg;

		} else if (Array.isArray(arg)) {
			classes += ' ' + classNames.apply(null, arg);

		} else if (argType === 'object') {
			for (var key in arg) {
				if (arg.hasOwnProperty(key) && arg[key]) {
					classes += ' ' + key;
				}
			}
		}
	}

	return classes.substr(1);
}

/*

 // safely export classNames for node / browserify
 if (typeof module !== 'undefined' && module.exports) {
 module.exports = classNames;
 }

 /!* global define *!/
 // safely export classNames for RequireJS
 if (typeof define !== 'undefined' && define.amd) {
 define('classnames', [], function() {
 return classNames;
 });
 }
 */
