const initConfig = {
	ROI: 97.3,
	INIT_AMOUNT: 100,
	INIT_EXPIRY: 15,
	CALL_FLAG: 5,
	PUT_FLAG: 6,
	INIT_CHART_PRICE: 'Bid',
	INIT_CHART_RES: 'Tick',
	INIT_CHART_TYPE: 'candlestick',
	INIT_CHART_PNTR: 'point',
	MIN_BET: 1,
	MAX_BET: 2500
};

export default initConfig;
