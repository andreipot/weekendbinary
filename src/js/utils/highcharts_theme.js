'use strict';

var theme = {
	chart: {
		backgroundColor: 'transparent',
		animation: false,
		//height: 560,
		margin: [0, 45, 25, 0],
		style: {
			fontFamily: 'OpenSans, Arial, Helvetica, sans-serif'
		},
		plotBorderColor: '#606063',
		panning: false
	},
	dateFormat: '%H:%M:%S',
	title: {
		text: null
	},
	subtitle: {
		enabled: false,
		style: {
			color: '#E0E0E3',
			textTransform: 'uppercase'
		}
	},
	xAxis: {
		type: 'datetime',
		tickPixelInterval: 150,
		gridLineWidth: 0,
		gridLineColor: 'transparent',
		minorGridLineColor: 'transparent',
		minorGridLineWidth: 0,
		minorTickLength: 5,
		minorTickWidth: 1,
		minorTickInterval: 'auto',
		minorTickColor: '#17161A',
		labels: {
			style: {
				color: '#9E9EA0',
				fontSize: '8px',
				fontWeight: 'bold'
			}
		},
		lineColor: '#17161A',
		tickColor: '#17161A',
		title: {
			text: null,
			style: {
				color: '#5b5b5b'
			}
		}
	},
	yAxis:
		{
			gridLineWidth: 0,
			gridLineColor: 'transparent',
			labels: {
				x: 25,
				y: 0,
				style: {
					color: '#ffffff',
					fontSize: '8px',
					fontWeight: 'bold'
				}
			},
			lineColor: '#17161A',
			minorGridLineColor: 'transparent',
			minorGridLineWidth: 0,
			tickColor: '#17161A',
			tickWidth: 1,
			minorTickInterval: 'auto',
			title: {
				text: null
			},
			opposite: true,
			plotLines: [
				{
					width: 1,
					zIndex: 6,
					label: {
						align: 'right',
						x: 40,
						y: 2,
						useHTML: true,
						style: {
							color: '#fff',
							fontSize: '8px',
							fontWeight: 'bold',
							padding: '3px 3px 3px 20px'
						}
					}
				}
			]
		},
	tooltip: {
		animation: false,
		xDateFormat: '%H:%M:%S',
		valueDecimals: 4,
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		shadow: false,
		borderWidth: 0,
		style: {
			color: '#F0F0F0',
			fontSize: '10px',
			padding: '2px 3px'
		},
		headerFormat: '<small>{point.key}</small>',
		pointFormat: '<span style="color:red; margin: auto 2px"> \u25B6 </span> {point.y}'
	},

	plotOptions: {
		series: {
			animation: false,
			dataLabels: {
				color: '#B0B0B3'
			},
			marker: {
				enabled: false,
				lineColor: '#333'
			},
			dataGrouping: {
				//forced: true
				enabled: false,
				dateTimeLabelFormats: {
					minute: ['%b %e, %H:%M', '%b %e, %H:%M', '-%H:%M']
				}
			}
		},
		boxplot: {
			fillColor: '#505053'
		},
		candlestick: {
			color: 'red',
			lineColor: 'red',
			upColor: '#00FF00',
			upLineColor: '#00FF00',
			cropThreshold: 40,
			tooltip: {
				xDateFormat: '%H:%M:%S',
				pointFormat: '<span style="color:red; margin: auto 2px"> \u25B6 </span> <b>O</b> {point.open} | <b>H</b> {point.high} | <b>L</b> {point.low} | <b>C</b> {point.close}',
			},
			pointWidth: 6
		},
		errorbar: {
			color: 'white'
		},
		areaspline: {
			color: '#089400',
			fillColor: {
				linearGradient: {
					x1: 0,
					y1: 0,
					x2: 0,
					y2: 1
				},
				stops: [
					[0, 'rgba(8, 148, 0, 1)'], [1, 'rgba(8, 148, 0, 0.25)']
				]
			},
			marker: {
				radius: 2
			},
			lineWidth: 1,
			states: {
				hover: {
					lineWidth: 1
				}
			},
			threshold: null,
			cropThreshold: 40
		}
	},

	legend: {
		enabled: false
	},
	exporting: {
		enabled: false
	},
	credits: {
		enabled: false
	},

	labels: {
		style: {
			color: '#707073'
		}
	},

	scrollbar: {
		enabled: false
	},

	rangeSelector: {
		enabled: false
	},

	navigator: {
		enabled: false,
		height: 30,
		margin: 0
	}
};

export default theme;
