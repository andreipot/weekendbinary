'use strict';

import Server from '../utils/server';
import getSid from '../utils/getsid';
import Cookies from 'cookies-js';


// todo - remove in production

window.checkMethod = function (url, param) {
	if (!url) return false;
	param = param || {};
	Server.send(url, param, function (r) {
		console.log(r);
	});
};

var serverSubscriptionId,
	quoteFeedSubscriptions = {},
	tradeOpenedId,
	tradeClosedId;

class Actions {
	// server actions

	// start socket client up
	// requested from AppLoader component on the page load
	initServer() {
		tradeOpenedId = Server.subOnServerData((tradeData, type) => {
			if (type === 'binary.order.add') this.actions.openTrade(tradeData);
		});
		tradeClosedId = Server.subOnServerData((tradeData, type) => {
			if (type === 'binary.order.update') {
				this.actions.closeTrade(tradeData);
			}
		});
		serverSubscriptionId = (Server.subOnConnect(() => {
			Server.send('market.symbols.get', {}, (r) => {
				if (r) {
					// replace AppLoader with the App component
					this.actions.initApp(r);
				} else {
					throw new Error('cannot connect to server');
				}
			});
		}));
		Server.connect();
	}

	initApp(appSymbolsConfig) {
		Server.unsubOnConnect(serverSubscriptionId);
		//noinspection Eslint
		var symbols = appSymbolsConfig['_list'];
		if (!symbols.length) {
			throw new Error('No symbols!', appSymbolsConfig);
		}
		// adding trade control panels per symbol
		var appSymbols = [];
		for (var i = 0; i < symbols.length; i++) {
			var currSym = symbols[i];
			appSymbols.push(appSymbolsConfig[currSym]);
		}
		this.dispatch(appSymbols);
	}


	// subscription requested once upon MainSection componentDidMount
	// unsubscribe upon componentWillUnmount

	subscribeToQuoteFeed(symbol, unsubscribe) {
		var serverMethod = 'feed.' + symbol + (unsubscribe ? '.unsub' : '.sub');
		//console.log(new Date().toLocaleTimeString(), serverMethod)
		if (!unsubscribe) {
			if (quoteFeedSubscriptions[symbol]) return;
			quoteFeedSubscriptions[symbol] = Server.subOnServerData((quoteData, type) => {
				if (type === 'quote' && quoteData[0].symbol === symbol) {
					this.actions.updateTicker(quoteData);
				}
			});
			Server.send(serverMethod);
		} else {
			Server.unsubOnServerData(quoteFeedSubscriptions[symbol]);
			delete quoteFeedSubscriptions[symbol];
		}
	}

	// requests for historical chart data
	getChart(symbol, timeframe) {
		var frame = timeframe || 'Tick',
			serverMethod = 'feed.' + symbol.toUpperCase() + '.charts.' + frame + '.get';
		Server.send(serverMethod, {}, (r) => {
			if (r.length) {
				this.dispatch({symbol: symbol, chartData: r});
				if (frame === 'Tick') {
					this.actions.initTicker({symbol: symbol, data: r});
				}
			} else {
				console.log('%c ' + symbol + ' ' + frame + ', resp:' + r, 'background: black; color: #fff');
			}
		});
	}

	setIndicator(indicatorSettings) {
		this.dispatch(indicatorSettings);
		this.actions.closeModal();
	}

	// ticker stuff

	initTicker(symDataObj) {
		this.dispatch(symDataObj);
	}

	// dispatch new quote to TickerStore and ChartStore

	updateTicker(quoteData) {
		this.dispatch(quoteData[0]);
	}

	updateStockChart(symbolchartDataObj) {
		//console.log(quoteData[0]);
		this.dispatch(symbolchartDataObj);
	}

	// chart settings radio groups

	setChartControl(valSymObj) {
		this.dispatch(valSymObj);
	}

	setChartZoom(valSymObj) {
		this.dispatch(valSymObj);
	}

	/*removeChartTab(valSymObj){
		this.dispatch(valSymObj);
	}*/

	// up / down buttons

	createTrade(reqData) {
		Server.send('binary.open.add', reqData, (r) => {
			if (!r) {
				Server.unsubOnServerData(tradeOpenedId);
				console.log('createTrade failed');
			}
		});
	}

	openTrade(tradeData) {
		this.dispatch(tradeData);
	}

	updateTrade(tradeData) {
		this.dispatch(tradeData);
	}

	closeTrade(tradeData) {
		this.dispatch(tradeData);
	}

	updateBalance(profitLoss) {
		this.dispatch(profitLoss);
	}

	// big round button

	requestEarlyExit(tradeId) {
		//console.time('EE request');
		if (!tradeId) return;
		console.log('EE request start, trade', tradeId);
		Server.send('binary.open.' + tradeId + '.earlyexit', null, (r) => {
			if (!r) {
				console.log('EE request failed');
			} else {
				console.log('EE request complete, trade', tradeId);
			}
			//console.timeEnd('EE request');
		});
	}

	// client actions

	setActiveSymbol(sym){
		this.dispatch(sym);
	}

	setTradeMode(mode){
		console.log(mode)
		this.dispatch(mode);
	}

	// user actions

	userLogin(loginData) {
		let userData = loginData,
			sid = getSid();
		userData.sid = sid;
		Server.send('traders.auth.set', userData, (r) => {
			//console.log(userData, r);
			if (r) {
				Cookies.set('sid', sid, {expires: 2592000});
				this.actions.userLoggedIn({username: r.name, userbalance: r.balance, sid: sid, login: r.login});
			} else {
				console.log('user login failed');
			}
		});
	}

	userLoggedIn(userData) {
		this.dispatch(userData);
		if(!userData) return;
		this.actions.getUserOpenTrades(userData.login);
	}

	getUserOpenTrades(login) {
		Server.send('binary.open.login.' + login + '.get', null, (r) => {
			if (r) this.dispatch(r);
			this.actions.getUserClosedTrades(login);
		});
	}

	getUserClosedTrades(login) {
		this.actions.setLoading(true);
		Server.send('binary.closed.login.' + login + '.get', null, (r) => {
			if (r) this.dispatch(r);
		});
	}

	userLogout() {
		var sid = Cookies.get('sid');
		Server.send('traders.auth.' + sid + '.delete', null, (r) => {
			if (r) {
				Cookies.expire('sid');
				this.actions.userLoggedIn(null);
			} else {
				console.log('user logout failed');
			}
		});
	}

	// slider

	setExpiryValue(valSymObj) {
		this.dispatch(valSymObj);
	}

	updateCountDown(symbol) {
		this.dispatch(symbol);
	}

	timerOff(symbol) {
		this.dispatch(symbol);
	}

	// amount input

	setAmountValue(valSymObj) {
		this.dispatch(valSymObj);
	}

	// trades table

	setTradeTableView(val) {
		this.dispatch(val);
	}

	setSelectedTrade(tradeData) {
		this.dispatch(tradeData);
		if(tradeData.symbol) this.actions.setActiveSymbol(tradeData.symbol);
	}

	// modal control

	openModal() {
		this.dispatch();
	}

	closeModal() {
		this.dispatch();
	}

	// template action

	templateAction(value) {
		this.dispatch(value);
	}

	setLoading(start){
		// hides the interface with loading animation
		var waitasec = document.getElementById('waitasec'),
			styl = (start) ? 'block' : 'none';
		waitasec.style.display = styl;
	}
}

export default alt.createActions(Actions);
