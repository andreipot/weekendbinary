var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	target: 'web',
	cache: true,
	debug: false,
	entry: ['./src/style/main_src.css', './src/js/bootstrap.jsx'],
	output: {
		path: './assets/js',
		filename: 'bundle.min.js',
		pathinfo: true
	},
	resolve: {
		extensions: ['', '.js', '.jsx']
	},
	noParse: [/utils/],
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: [/node_modules/, /assets/, /vendor/],
				loader: 'babel',
				query: {
					optional: ['runtime'],
					stage: 0
				}
			},
			{
				test: /\.css$/,
				exclude: /node_modules/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader?minimize!cssnext-loader')
			},
			{
				test: /\.png$/,
				loader: 'url-loader?limit=15000&mimetype=image/png&name=../img/[name].[ext]'
			},
			{
				test: /\.(otf|eot|svg|ttf|woff|woff2)$/,
				loader: 'url-loader?limit=10000&name=../fonts/[name].[ext]'
			},
			{
				test: /\.jpg$/,
				loader: 'file-loader?name=../img/[name].[ext]'
			}
		]
	},
	plugins: [
		new webpack.optimize.UglifyJsPlugin({minimize: true, output: {comments: false}, mangle: true,
			compress: {
				sequences: true,
				dead_code: true,
				conditionals: true,
				booleans: true,
				unused: true,
				if_return: true,
				join_vars: true,
				drop_console: true
			}
		}),
		new ExtractTextPlugin('../css/main.css', {allChunks: true})
	],
	externals: {
		'react': 'React'
	}
};
